# Mentoring-HTML-CSS
## Name: Riza

### Achievements:
- Re-organized something to make it work better.
- Identified a problem and solved it.
- Came up with a new idea that improved things.

### Other:
- Logical thinker
- Quick learner
- Updated info

### Proud Achievements:
- [Education Achievement]
- [Personal Project Accomplishment]
- [Any other accomplishment you'd like to highlight]
